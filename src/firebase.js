import firebase from "firebase";



const firebaseApp = firebase.initializeApp ({
    apiKey: "AIzaSyD79_Z78FdK7-7F-28-DH6Vk9R0xDuBPCg",
    authDomain: "instagram-clone-e12bb.firebaseapp.com",
    databaseURL: "https://instagram-clone-e12bb.firebaseio.com",
    projectId: "instagram-clone-e12bb",
    storageBucket: "instagram-clone-e12bb.appspot.com",
    messagingSenderId: "450181067578",
    appId: "1:450181067578:web:416b37bd8df626437ad603",
    measurementId: "G-WYRZX5ZHGG"

});


  const db = firebaseApp.firestore();
  const auth = firebase.auth();
  const storage = firebase.storage();

  export {db, auth, storage};
